#include <SDL2/SDL.h>

#include <cstdio>
#include <cstdint>
#include <cmath>

#include "platform.h"
#include "math_lib.h"

#include "renderer.cpp"
#include "os_utils.cpp"

#include "asset.cpp"

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#define RENDER_SCALE 1.0
#define BACK_BUFFER_WIDTH Floor((f32)WINDOW_WIDTH * RENDER_SCALE)
#define BACK_BUFFER_HEIGHT Floor((f32)WINDOW_HEIGHT * RENDER_SCALE)

#define TARGET_FPS 60

#define TEMP_MEMORY_CAPACITY Megabytes(32)

struct CameraState
{
    vec3 position;
    vec3 velocity;
    vec3 rotation;
};

int main(int argc, char **argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        printf("SDL_Init Error: %s\n", SDL_GetError());
        return 1;
    }

    f64 targetMillisecondsPerFrame = 1000.0 / TARGET_FPS;
    f32 dt = 1.0f / TARGET_FPS;
    u64 frequency = SDL_GetPerformanceFrequency();

    SDL_Window *window = SDL_CreateWindow("Hello World!", 0, 0,
        WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        printf("SDL_CreateWindow Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Surface *backBuffer = SDL_CreateRGBSurface(
        0, BACK_BUFFER_WIDTH, BACK_BUFFER_HEIGHT, 32, 0, 0, 0, 0);

    PixelBuffer pixelBuffer = {};
    pixelBuffer.pixels = backBuffer->pixels;
    pixelBuffer.width = backBuffer->w;
    pixelBuffer.height = backBuffer->h;
    pixelBuffer.pitch = backBuffer->pitch;
    pixelBuffer.bytesPerPixel = backBuffer->format->BytesPerPixel;

    void *tempMemory = OS_AllocateMemory(TEMP_MEMORY_CAPACITY);
    Assert(tempMemory);

    MemoryArena tempArena = {};
    MemoryArena_Initialize(&tempArena, tempMemory, TEMP_MEMORY_CAPACITY);

    PixelBuffer zBuffer = {};
    zBuffer.pixels =
        AllocateArray(&tempArena, f32, BACK_BUFFER_WIDTH * BACK_BUFFER_HEIGHT);
    zBuffer.width = BACK_BUFFER_WIDTH;
    zBuffer.height = BACK_BUFFER_HEIGHT;
    zBuffer.pitch = BACK_BUFFER_WIDTH * sizeof(f32);
    zBuffer.bytesPerPixel = sizeof(f32);

    DebugReadFileResult fileResult = OS_ReadEntireFile("../content/monkey.obj");
    Assert(fileResult.contents != NULL);

    // -1 to remove nul character
    MeshData monkeyMesh = LoadObjFile((char *)fileResult.contents,
        fileResult.size - 1, &tempArena, &tempArena);

    MeshData cubeMesh = CreateCubeMeshData(&tempArena);

    DebugReadFileResult textureFile = OS_ReadEntireFile("../content/grass.jpg");
    Assert(textureFile.contents != NULL);
    PixelBuffer texture =
        LoadTexture(textureFile.contents, textureFile.size, &tempArena);

    CameraState camera = {};
    camera.position = Vec3(0, 0, 5);

    b32 running = true;
    u64 start = SDL_GetPerformanceCounter();
    f32 t = 0.0f;
    while (running)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                break;
            default:
                break;
            }
        }

        if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
        {
            SDL_SetRelativeMouseMode(SDL_TRUE);
        }
        else
        {
            SDL_SetRelativeMouseMode(SDL_FALSE);
        }

        f32 cameraForward = 0.0f;
        f32 cameraRight = 0.0f;
        const u8* keyboardState = SDL_GetKeyboardState(NULL);
        if (keyboardState[SDL_SCANCODE_W])
        {
            cameraForward = -1.0f;
        }
        if (keyboardState[SDL_SCANCODE_S])
        {
            cameraForward = 1.0f;
        }
        if (keyboardState[SDL_SCANCODE_A])
        {
            cameraRight = -1.0f;
        }
        if (keyboardState[SDL_SCANCODE_D])
        {
            cameraRight = 1.0f;
        }

        i32 mouseRelX = 0;
        i32 mouseRelY = 0;
        u32 mouseButtonState =
            SDL_GetRelativeMouseState(&mouseRelX, &mouseRelY);

        f32 mouseX = (f32)mouseRelX / (f32)WINDOW_WIDTH;
        f32 mouseY = (f32)mouseRelY / (f32)WINDOW_HEIGHT;

        vec3 newRotation = Vec3(-mouseY, -mouseX, 0.0f);

        vec3 rotation = camera.rotation;

        if (SDL_BUTTON(SDL_BUTTON_RIGHT) & mouseButtonState)
        {
            rotation += newRotation;
        }

        rotation.x = Clamp(rotation.x, -PI * 0.5f, PI * 0.5f);

        if (rotation.y > 2.0f * PI)
        {
            rotation.y -= 2.0f * PI;
        }
        if (rotation.y < 2.0f * -PI)
        {
            rotation.y += 2.0f * PI;
        }

        mat4 rotationMatrix = RotateY(rotation.y) * RotateX(rotation.x);

        vec3 forward = (rotationMatrix * Vec4(0, 0, 1, 0)).xyz;
        vec3 right = (rotationMatrix * Vec4(1, 0, 0, 0)).xyz;

        vec3 targetDir =
            SafeNormalize(forward * cameraForward + right * cameraRight);

        f32 speed = 200.0f;
        f32 friction = 7.0f;
        vec3 velocity = camera.velocity;
        velocity -= camera.velocity * friction * dt;
        velocity += targetDir * speed * dt;

        camera.velocity = velocity;
        camera.position = camera.position + velocity * dt;
        camera.rotation = rotation;

        t += 0.5f * PI * dt;
        if (t > 2.0f * PI)
        {
            t -= 2.0f * PI;
        }

        // Update display
        Clear(pixelBuffer, 0, 0, 0, 255);
        ClearF32(zBuffer, 1.0f);

        mat4 projection = Perspective(50.0f,
            (f32)BACK_BUFFER_WIDTH / (f32)BACK_BUFFER_HEIGHT, 0.001f, 100.0f);
        mat4 view = RotateX(-camera.rotation.x) * RotateY(-camera.rotation.y) *
                    Translate(-camera.position);

        mat4 viewProjection = projection * view;

#if 0
        DrawLine(pixelBuffer, Vec3(0, 0, 0), Vec3(1, 0, 0), Vec4(1, 0, 0, 1),
            viewProjection);
        DrawLine(pixelBuffer, Vec3(0, 0, 0), Vec3(0, 1, 0), Vec4(0, 1, 0, 1),
            viewProjection);
        DrawLine(pixelBuffer, Vec3(0, 0, 0), Vec3(0, 0, 1), Vec4(0, 0, 1, 1),
            viewProjection);
#endif

#if 1
        vec3 positions[] = {
            {-0.5f, -0.5f, 0.0f},
            {0.0f, 0.5f, 0.0f},
            {0.5f, -0.5f, 0.0f}
        };
        vec3 normals[] = {
            {0.0f, 0.0f, 1.0f},
            {0.0f, 0.0f, 1.0f},
            {0.0f, 0.0f, 1.0f},
        };
        vec3 colors[] = {
            {1.0, 0.0, 0.0},
            {0.0, 1.0, 0.0},
            {0.0, 0.0, 1.0}
        };
        vec2 textureCoordinates[] = {
            {0.0, 0.0},
            {0.5, 1.0},
            {1.0, 0.0}
        };
        u32 indices[] = { 0, 2, 1 };
        DrawTriangles(pixelBuffer, zBuffer, positions, normals, colors,
            textureCoordinates, indices, 3, texture, Vec4(0.18, 0.18, 0.18, 1),
            Identity(), view, projection);
#else
        DrawTriangles(pixelBuffer, zBuffer, monkeyMesh.positions,
            monkeyMesh.normals, monkeyMesh.indices, monkeyMesh.indexCount,
            Vec4(0.18, 0.18, 0.18, 1), Identity(), view, projection);
        mat4 model = Translate(Vec3(0, -0.5, 0)) * Scale(Vec3(1, 0.2, 1));
        DrawTriangles(pixelBuffer, zBuffer, cubeMesh.positions,
            cubeMesh.normals, cubeMesh.indices, cubeMesh.indexCount,
            Vec4(0.4, 1.0, 0.1, 1.0), model, view, projection);
        //DrawTrianglesNormals(pixelBuffer, cubeMesh.positions, cubeMesh.indices,
            //cubeMesh.indexCount, Vec4(1, 0, 1, 1), viewProjection * model);
#endif

        f64 millisecondsElapsed = ((SDL_GetPerformanceCounter() - start) / (f64)frequency) * 1000.0;

        if (millisecondsElapsed < targetMillisecondsPerFrame)
        {
            f64 sleepError = 0.0; // So little sleep error we can maybe get by without busy waiting
            f64 sleepTime = targetMillisecondsPerFrame - millisecondsElapsed;

            sleepTime -= sleepError;
            if (sleepTime >= 1)
            {
                SDL_Delay((u32)sleepTime);
            }
        }
        millisecondsElapsed = ((SDL_GetPerformanceCounter() - start) / (f64)frequency) * 1000.0;
        printf("millisecondsElapsed: %g\n", millisecondsElapsed);

        SDL_Surface *windowSurface = SDL_GetWindowSurface(window);
        SDL_BlitScaled(backBuffer, NULL, windowSurface, NULL);
        SDL_UpdateWindowSurface(window);
        start = SDL_GetPerformanceCounter();
    }

    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
