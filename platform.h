#pragma once

#include <stdint.h>
#include <stdarg.h>

#define global static
#define internal static
#define local_persist static

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint32_t b32;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

#define PI 3.14159265359f
#undef NULL
#define NULL 0

#define Kilobytes(X) ((X) * 1024)
#define Megabytes(X) (Kilobytes((X) * 1024))
#define Gigabytes(X) (Megabytes((X) * 1024ll))
#define Terabytes(X) (Gigabytes((X) * 1024ll))

#define ArrayCount(ARRAY) (sizeof(ARRAY) / sizeof((ARRAY)[0]))

#define Assert(Expression) { if(!(Expression)) *(volatile int*)NULL = 0; }
#define InvalidCodePath() Assert(!"Invalid code path")

#define OffsetOf(STRUCT, MEMBER) (size_t)(&((STRUCT *)0)->MEMBER)

#define Bit(N) (1 << N)
#define BoolToString(X) (X ? "true" : "false")

#define SwapValues(A, B, TYPE) \
    TYPE _temp = A; \
    A = B; \
    B = _temp;

#define U8_MAX  0xFF
#define I16_MAX 0x7FFF
#define U16_MAX 0xFFFF
#define U32_MAX 0xFFFFFFFF

inline u32 SafeTruncateU64ToU32(u64 x)
{
    Assert(x <= U32_MAX);
    u32 result = (u32)x;
    return result;
}

inline u16 SafeTruncateU32ToU16(u32 x)
{
    Assert(x <= U16_MAX);
    u16 result = (u16)x;
    return result;
}

inline u8 SafeTruncateU32ToU8(u32 x)
{
    Assert(x <= U8_MAX);
    u8 result = (u8)x;
    return result;
}

// NOTE: This might not be best for all platforms, still not as fast as memset
// which is likely using intrinsics.
inline void ClearToZero(void *buffer, u64 length)
{
    u64 k = length / sizeof(u64);
    for (u64 i = 0; i < k; ++i)
    {
        *((u64 *)buffer + i) = 0;
    }

    u64 j = length % sizeof(u64);
    for (u64 i = 0; i < j; ++i)
    {
        *((u8 *)buffer + i + sizeof(u64) * k) = 0;
    }
}

// FIXME: This is clashing with CopyMemory defined in window.h
#undef CopyMemory
// TODO: We can surely copy more than 1 byte at a time?
inline void CopyMemory(void *dst, const void *src, u64 length)
{
    for (u64 i = 0; i < length; ++i)
    {
        *((u8 *)dst + i) = *((u8 *)src + i);
    }
}

inline bool IsWhitespace(char c) { return (c == ' ' || c == '\t' || c == '\n'); }

inline bool IsNumeric(char c)
{
    return ((c >= '0' && c <= '9') || (c == '-' || c == '+' || c == '.'));
}


struct MemoryArena
{
    void *base;
    u64 size;
    u64 capacity;
};

inline void MemoryArena_Initialize(MemoryArena *arena, void *memory, u64 capacity)
{
    arena->base = memory;
    arena->size = 0;
    arena->capacity = capacity;
}

inline void *MemoryArena_Allocate(MemoryArena *arena, u64 size)
{
    Assert(arena->size + size < arena->capacity);
    void *result = (u8 *)arena->base + arena->size;
    arena->size += size;
    return result;
}

inline void MemoryArena_Partition(MemoryArena *parent, MemoryArena *child, u64 size)
{
    void *childMemory = MemoryArena_Allocate(parent, size);
    MemoryArena_Initialize(child, childMemory, size);
}

inline b32 MemoryArena_CanAllocate(MemoryArena *arena, u64 size)
{
    return (arena->size + size < arena->capacity);
}

#define AllocateArray(ARENA, TYPE, LENGTH) \
    (TYPE *)MemoryArena_Allocate(ARENA, sizeof(TYPE) * LENGTH)

#define AllocateStruct(ARENA, TYPE) \
    (TYPE *)MemoryArena_Allocate(ARENA, sizeof(TYPE))

#define CanAllocateArray(ARENA, TYPE, LENGTH) \
    MemoryArena_CanAllocate(ARENA, sizeof(TYPE) * LENGTH)

inline void ConcatStrings(char *sourceA, u32 lengthA, char *sourceB, u32 lengthB,
    char *dest, u32 destLength)
{
    Assert(lengthA + lengthB < destLength);
    for (u32 i = 0; i < lengthA; ++i)
    {
        *dest++ = *sourceA++;
    }

    for (u32 i = 0; i < lengthB; ++i)
    {
        *dest++ = *sourceB++;
    }
}

inline u32 StringLength(const char *string)
{
    u32 count = 0;
    while (*string++)
    {
        count++;
    }

    return count;
}

inline b32 StringCompare(const char *a, const char *b)
{
    Assert(a != NULL);
    Assert(b != NULL);

    b32 result = true;
    if (a != NULL && b != NULL)
    {
        while (*a != '\0' && *b != '\0')
        {
            if (*a != *b)
            {
                result = false;
                break;
            }

            a++;
            b++;
        }
    }

    return result;
}

inline b32 StringCompare(const char *a, const char *b, u32 length)
{
    Assert(a != NULL);
    Assert(b != NULL);

    b32 result = true;
    if (a != NULL && b != NULL)
    {
        u32 i = 0;
        for (i = 0; i < length; ++i)
        {
            if (a[i] != b[i])
            {
                result = false;
                break;
            }

            if ((a[i] == '\0') || (b[i] == '\0'))
            {
                break;
            }
        }
    }

    return result;
}

inline u32 StringCopy(char *dst, u32 capacity, const char *src)
{
    u32 length = StringLength(src);
    Assert(length < capacity - 1);

    CopyMemory(dst, src, length);
    dst[length] = '\0';

    return length;
}

struct DataBuffer
{
    u8 *start;
    u32 cursor;
    u32 capacity;
};

#define ASSERT_ON_SERIALIZATION_FAILURE

inline DataBuffer CreateDataBuffer(void *buffer, u32 capacity)
{
    DataBuffer result = {};
    result.start = (u8 *)buffer;
    result.capacity = capacity;

    return result;
}

inline b32 WriteData(DataBuffer *buffer, void *data, u32 length)
{
    b32 result = false;
    if (buffer->cursor + length <= buffer->capacity)
    {
        CopyMemory(buffer->start + buffer->cursor, data, length);
        buffer->cursor += length;
        result = true;
    }
#ifdef ASSERT_ON_SERIALIZATION_FAILURE
    Assert(result);
#endif
    return result;
}

inline b32 ReadData(DataBuffer *buffer, void *data, u32 length)
{
    b32 result = false;
    if (buffer->cursor + length <= buffer->capacity)
    {
        CopyMemory(data, buffer->start + buffer->cursor, length);
        buffer->cursor += length;
        result = true;
    }
#ifdef ASSERT_ON_SERIALIZATION_FAILURE
    Assert(result);
#endif
    return result;
}

#define WriteValue(BUFFER, VALUE) \
    WriteData(BUFFER, &VALUE, sizeof(VALUE))
#define WriteArray(BUFFER, ARRAY, LENGTH) \
    WriteData(BUFFER, ARRAY, sizeof(ARRAY[0]) * LENGTH)

#define ReadValue(BUFFER, VALUE) \
    ReadData(BUFFER, VALUE, sizeof(*VALUE))
#define ReadArray(BUFFER, ARRAY, LENGTH) \
    ReadData(BUFFER, ARRAY, sizeof(ARRAY[0]) * LENGTH)
