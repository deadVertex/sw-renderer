#!/bin/bash

COMPILER_FLAGS="-g -O2 -Wall -Werror -Wno-missing-braces -Wno-unused-function -Wno-unused-variable"

mkdir -p build
clang++ $COMPILER_FLAGS main.cpp -o build/sw-renderer $(pkg-config --cflags --libs sdl2)
