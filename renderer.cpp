struct PixelBuffer
{
    void *pixels;
    u32 width;
    u32 height;
    u32 pitch;
    u32 bytesPerPixel;
};

#define SetPixel(OUTPUT, X, Y, COLOR)                                          \
    *(u32 *)((u8 *)OUTPUT.pixels + Y * OUTPUT.pitch +                          \
             X * OUTPUT.bytesPerPixel) = COLOR

#define ReadPixelU32(OUTPUT, X, Y)                                             \
    *(u32 *)((u8 *)OUTPUT.pixels + Y * OUTPUT.pitch + X * OUTPUT.bytesPerPixel)

#define ReadPixelF32(OUTPUT, X, Y)                                             \
    *(f32 *)((u8 *)OUTPUT.pixels + Y * OUTPUT.pitch + X * OUTPUT.bytesPerPixel)

#define WritePixelF32(OUTPUT, X, Y, VALUE)                                     \
    *(f32 *)((u8 *)OUTPUT.pixels + Y * OUTPUT.pitch +                          \
             X * OUTPUT.bytesPerPixel) = VALUE

inline u32 GetColor32(vec4 color)
{
    u8 r = SafeTruncateU32ToU8((u32)(color.r * 255.0f));
    u8 g = SafeTruncateU32ToU8((u32)(color.g * 255.0f));
    u8 b = SafeTruncateU32ToU8((u32)(color.b * 255.0f));
    u8 a = SafeTruncateU32ToU8((u32)(color.a * 255.0f));
    u32 result = (a << 24) | (r << 16) | (g << 8) | b;

    return result;
}

inline void Clear(PixelBuffer output, u8 r, u8 g, u8 b, u8 a)
{
    // AARRGGBB
    u32 color = (a << 24) | (r << 16) | (g << 8) | b;
    for (u32 y = 0; y < output.height; ++y)
    {
        u32 *rowPointer = (u32*)((u8 *)output.pixels + y * output.pitch);
        for (u32 x = 0; x < output.width; ++x)
        {
            u32 *pixel = rowPointer + x;
            *pixel = color;
        }
    }
}

inline void ClearF32(PixelBuffer output, f32 value)
{
    for (u32 y = 0; y < output.height; ++y)
    {
        f32 *rowPointer = (f32 *)((u8 *)output.pixels + y * output.pitch);
        for (u32 x = 0; x < output.width; ++x)
        {
            f32 *pixel = rowPointer + x;
            *pixel = value;
        }
    }
}

// Bresenham line drawing
inline void DrawLine(PixelBuffer output, i32 x0, i32 y0, i32 x1, i32 y1, u32 color)
{
    i32 dx = x1 - x0;
    i32 dy = y1 - y0;
    f32 m = (f32)dy / (f32)dx;
    f32 offset = 0.0f;
    f32 threshold = 0.5f;
    if (m <= 1 && m >= -1)
    {
        f32 delta = Abs(m);

        i32 y = y0;
        i32 adjust = m >= 0 ? 1 : -1;

        if (x0 > x1)
        {
            i32 temp = x0;
            x0 = x1;
            x1 = temp;
            y = y1;
        }

        x0 = Max(x0, 0);
        x1 = Min(x1, output.width - 1);
        y = Clamp(y, 0, output.height - 1);

        for (i32 x = x0; x <= x1; ++x)
        {
            SetPixel(output, x, y, color);
            offset += delta;
            if (offset >= threshold)
            {
                y += adjust;
                threshold += 1.0f;
            }
        }
    }
    else
    {
        f32 delta = Abs((f32)dx / (f32)dy);

        i32 x = x0;
        i32 adjust = m >= 0 ? 1 : -1;

        if (y0 > y1)
        {
            i32 temp = y0;
            y0 = y1;
            y1 = temp;
            x = x1;
        }

        y0 = Max(y0, 0);
        y1 = Min(y1, output.height - 1);
        x = Clamp(x, 0, output.width - 1);

        for (i32 y = y0; y <= y1; ++y)
        {
            SetPixel(output, x, y, color);
            offset += delta;
            if (offset >= threshold)
            {
                x += adjust;
                threshold += 1.0f;
            }
        }
    }
}

inline void DrawTrianglesWireframe(PixelBuffer output, vec3 *positions,
    u32 *indices, u32 indexCount, vec4 color, mat4 mvp)
{
    Assert(indexCount % 3 == 0);
    u32 color32 = GetColor32(color);
    for (u32 triangleIndex = 0; triangleIndex < indexCount / 3; ++triangleIndex)
    {
        u32 i = indices[triangleIndex * 3];
        u32 j = indices[triangleIndex * 3 + 1];
        u32 k = indices[triangleIndex * 3 + 2];

        vec3 v0 = positions[i];
        vec3 v1 = positions[j];
        vec3 v2 = positions[k];

        vec4 p0 = mvp * Vec4(v0, 1.0);
        vec4 p1 = mvp * Vec4(v1, 1.0);
        vec4 p2 = mvp * Vec4(v2, 1.0);

        p0 = p0 * (1.0f / p0.w);
        p1 = p1 * (1.0f / p1.w);
        p2 = p2 * (1.0f / p2.w);

        i32 x0 = (p0.x * 0.5f + 0.5f) * output.width;
        i32 y0 = (p0.y * 0.5f + 0.5f) * output.height;

        i32 x1 = (p1.x * 0.5f + 0.5f) * output.width;
        i32 y1 = (p1.y * 0.5f + 0.5f) * output.height;

        i32 x2 = (p2.x * 0.5f + 0.5f) * output.width;
        i32 y2 = (p2.y * 0.5f + 0.5f) * output.height;

        y0 = output.height - y0;
        y1 = output.height - y1;
        y2 = output.height - y2;

        DrawLine(output, x0, y0, x1, y1, color32);
        DrawLine(output, x1, y1, x2, y2, color32);
        DrawLine(output, x2, y2, x0, y0, color32);
    }
}

inline void DrawTriangles(PixelBuffer output, PixelBuffer zBuffer,
    vec3 *positions, vec3 *normals, vec3 *colors, vec2 *textureCoordinates,
    u32 *indices, u32 indexCount, PixelBuffer texture, vec4 color, mat4 model,
    mat4 view, mat4 projection)
{
    mat4 mvp = projection * view * model;
    mat4 modelView = view * model;
    vec3 lightDirection = SafeNormalize(Vec3(-0.2, 0.5, 1.0));
    Assert(indexCount % 3 == 0);
    for (u32 triangleIndex = 0; triangleIndex < indexCount / 3; ++triangleIndex)
    {
        u32 i = indices[triangleIndex * 3];
        u32 j = indices[triangleIndex * 3 + 1];
        u32 k = indices[triangleIndex * 3 + 2];

        vec3 v0 = positions[i];
        vec3 v1 = positions[j];
        vec3 v2 = positions[k];

        // TODO: Transform normals into world space
        vec3 n0 = normals[i];
        vec3 n1 = normals[j];
        vec3 n2 = normals[k];

        vec3 c0 = colors[i];
        vec3 c1 = colors[j];
        vec3 c2 = colors[k];

        vec2 t0 = textureCoordinates[i];
        vec2 t1 = textureCoordinates[j];
        vec2 t2 = textureCoordinates[k];

        vec3 faceNormal = SafeNormalize(Cross(v1 - v0, v2 - v0));
        // Transform to view space
        faceNormal = TransformVector(faceNormal, modelView);

        // Back-face culling
        if (Dot(faceNormal, Vec3(0, 0, 1)) >= 0.0f)
        {
            vec4 p0 = mvp * Vec4(v0, 1.0);
            vec4 p1 = mvp * Vec4(v1, 1.0);
            vec4 p2 = mvp * Vec4(v2, 1.0);

            p0 = p0 * (1.0f / p0.w);
            p1 = p1 * (1.0f / p1.w);
            p2 = p2 * (1.0f / p2.w);

            i32 x0 = (p0.x * 0.5f + 0.5f) * output.width;
            i32 y0 = (p0.y * 0.5f + 0.5f) * output.height;

            i32 x1 = (p1.x * 0.5f + 0.5f) * output.width;
            i32 y1 = (p1.y * 0.5f + 0.5f) * output.height;

            i32 x2 = (p2.x * 0.5f + 0.5f) * output.width;
            i32 y2 = (p2.y * 0.5f + 0.5f) * output.height;

            y0 = output.height - y0;
            y1 = output.height - y1;
            y2 = output.height - y2;

            i32 xmin = Min(x0, Min(x1, x2));
            i32 xmax = Max(x0, Max(x1, x2));

            i32 ymin = Min(y0, Min(y1, y2));
            i32 ymax = Max(y0, Max(y1, y2));

            xmin = Max(xmin, 0);
            xmax = Min(xmax, output.width - 1);
            ymin = Max(ymin, 0);
            ymax = Min(ymax, output.height - 1);

            for (i32 y = ymin; y <= ymax; ++y)
            {
                for (i32 x = xmin; x <= xmax; ++x)
                {
                    // Compute barycentric coordinates for (x,y)
                    vec3 u = Cross(Vec3(x2 - x0, x1 - x0, x0 - x),
                        Vec3(y2 - y0, y1 - y0, y0 - y));
                    if (Abs(u.z) >= 1.0f)
                    {
                        vec3 b = Vec3(
                            1.0f - (u.x + u.y) / u.z, u.y / u.z, u.x / u.z);

                        if (b.x >= 0.0f && b.y >= 0.0f && b.z >= 0.0f)
                        {
                            f32 depth = p0.z * b.x + p1.z * b.y + p2.z * b.z;
                            if (depth >= 0.0f && depth < 1.0f)
                            {
                                f32 storedDepth = ReadPixelF32(zBuffer, x, y);
                                if (depth < storedDepth)
                                {
                                    // Fragment shader
                                    vec3 normal = SafeNormalize(
                                        n0 * b.x + n1 * b.y + n2 * b.z);

                                    f32 diffuseTerm =
                                        Max(Dot(normal, lightDirection), 0.0);
                                    diffuseTerm = Min(diffuseTerm + 0.2f, 1.0f);
                                    u32 color32 =
                                        GetColor32(color * diffuseTerm);

                                    vec3 vertexColor =
                                        c0 * b.x + c1 * b.y + c2 * b.z;

                                    vec2 uv = t0 * b.x + t1 * b.y + t2 * b.z;
                                    i32 tx = (i32)(uv.x * texture.width);
                                    i32 ty = (i32)(uv.y * texture.height);
                                    tx = Clamp(tx, 0, texture.width - 1);
                                    ty = Clamp(ty, 0, texture.height - 1);

                                    u32 textureColor =
                                        ReadPixelU32(texture, tx, ty);

                                    WritePixelF32(zBuffer, x, y, depth);
                                    SetPixel(output, x, y, textureColor);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

inline void DrawLine(
    PixelBuffer output, vec3 start, vec3 end, vec4 color, mat4 mvp)
{
    vec4 p0 = mvp * Vec4(start, 1.0);
    vec4 p1 = mvp * Vec4(end, 1.0);

    p0 = p0 * (1.0f / p0.w);
    p1 = p1 * (1.0f / p1.w);

    i32 x0 = (p0.x * 0.5f + 0.5f) * output.width;
    i32 y0 = (p0.y * 0.5f + 0.5f) * output.height;

    i32 x1 = (p1.x * 0.5f + 0.5f) * output.width;
    i32 y1 = (p1.y * 0.5f + 0.5f) * output.height;

    y0 = output.height - y0;
    y1 = output.height - y1;

    DrawLine(output, x0, y0, x1, y1, GetColor32(color));
}

inline void DrawTrianglesNormals(PixelBuffer output, vec3 *positions,
    u32 *indices, u32 indexCount, vec4 color, mat4 mvp)
{
    Assert(indexCount % 3 == 0);
    for (u32 triangleIndex = 0; triangleIndex < indexCount / 3; ++triangleIndex)
    {
        u32 i = indices[triangleIndex * 3];
        u32 j = indices[triangleIndex * 3 + 1];
        u32 k = indices[triangleIndex * 3 + 2];

        vec3 v0 = positions[i];
        vec3 v1 = positions[j];
        vec3 v2 = positions[k];

        vec3 faceNormal = SafeNormalize(Cross(v1 - v0, v2 - v0));

        vec3 center = (v0 + v1 + v2) * (1.0f / 3.0f);

        DrawLine(output, center, center + faceNormal * 0.5f, color, mvp);
    }
}
