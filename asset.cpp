// TODO: Look at replacing this with our own PNG loading code
#define STB_NO_STDIO
#define STBI_ONLY_JPEG
#define STBI_NO_SIMD
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

enum
{
    VertexData_Positions = 0x1,
    VertexData_Normals = 0x2,
    VertexData_TextureCoordinates = 0x4,
};

struct MeshData
{
    u32 *indices;
    vec3 *positions;
    vec3 *normals;
    vec2 *textureCoordinates;
    u32 vertexCount;
    u32 indexCount;
    u32 flags;
};

// TODO: Replace atoi and atof
// TODO: Optimize vertex and index lists, remove duplicate vertices
internal MeshData LoadObjFile(char *fileData, u32 length,
    MemoryArena *meshDataArena, MemoryArena *tempArena)
{
    u32 maxVertices = 0xFFFF;
    vec3 *positions = AllocateArray(tempArena, vec3, maxVertices);
    vec3 *normals = AllocateArray(tempArena, vec3, maxVertices);
    vec2 *texCoords = AllocateArray(tempArena, vec2, maxVertices);
    u32 positionCount = 0;
    u32 normalCount = 0;
    u32 texCoordCount = 0;

    u32 maxIndices = 0xFFFF;
    u32 *indices = AllocateArray(tempArena, u32, maxIndices);
    u32 indexCount = 0;
    u32 vertexCount = 0;

    vec3 *vertexPositions = AllocateArray(tempArena, vec3, maxVertices);
    vec3 *vertexNormals = AllocateArray(tempArena, vec3, maxVertices);
    vec2 *vertexTextureCoordinates =
        AllocateArray(tempArena, vec2, maxVertices);

    b32 useTexCoords = false;

    u32 cursor = 0;
    while (cursor < length)
    {
        // Consume whitespace
        while(IsWhitespace(fileData[cursor]))
        {
            // Remove any preceeding whitespace
            cursor++;
        }

        // Skip comments and objects for now
        if (fileData[cursor] == '#' ||
            fileData[cursor] == 'o')
        {
            while (fileData[cursor++] != '\n')
            {
                // Skip entire line
            }
            continue;
        }

        if (fileData[cursor] == 'v')
        {
            // Handle vertex
            cursor++;
            if (fileData[cursor] == ' ')
            {
                // vertex position

                Assert(positionCount < maxVertices);
                vec3 *position = positions + positionCount++;

                // Parse 3 floats
                // Consume non-numeric characters
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                position->x = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over x value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                position->y = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over y value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                position->z = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over z value
                    cursor++;
                }
            }
            else if (fileData[cursor] == 'n')
            {
                // vertex normal
                Assert(normalCount < maxVertices);
                vec3 *normal = normals + normalCount++;

                // Parse 3 floats
                // Consume non-numeric characters
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                normal->x = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over x value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                normal->y = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over y value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                normal->z = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over z value
                    cursor++;
                }
            }
            else if (fileData[cursor] == 't')
            {
                // vertex texture coordinates
                useTexCoords = true;

                Assert(texCoordCount < maxVertices);
                vec2 *texCoord = texCoords + texCoordCount++;

                // Parse 2 floats
                // Consume non-numeric characters
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                texCoord->x = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over x value
                    cursor++;
                }

                while (IsWhitespace(fileData[cursor]))
                {
                    // Skip over whitespace
                    cursor++;
                }

                texCoord->y = (float)atof(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // Skip over y value
                    cursor++;
                }
            }
            else
            {
                // ignore other vertex data for now
            }
        }
        else if (fileData[cursor] == 'f')
        {
            cursor++;
            while (IsWhitespace(fileData[cursor]))
            {
                // Skip over whitespace
                cursor++;
            }

            for (u32 index = 0; index < 3; ++index)
            {
                while (!IsNumeric(fileData[cursor]))
                {
                    cursor++;
                }

                u32 positionIndex = (u32)atoi(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // skip over position index
                    cursor++;
                }

                if (fileData[cursor] == '/')
                {
                    cursor++;
                }

                u32 texCoordIndex = 0;
                if (useTexCoords)
                {
                    texCoordIndex = (u32)atoi(fileData + cursor);
                    while (IsNumeric(fileData[cursor]))
                    {
                        // skip over texCoord index
                        cursor++;
                    }
                }

                if (fileData[cursor] == '/')
                {
                    cursor++;
                }

                u32 normalIndex = (u32)atoi(fileData + cursor);
                while (IsNumeric(fileData[cursor]))
                {
                    // skip over normal index
                    cursor++;
                }

                // Convert to 0 base index
                Assert(positionIndex > 0);
                Assert(normalIndex > 0);

                positionIndex -= 1;
                normalIndex -= 1;

                Assert(vertexCount < maxVertices);
                vertexPositions[vertexCount] = positions[positionIndex];

                Assert(normalIndex < maxVertices);
                vertexNormals[vertexCount] = normals[normalIndex];

                if (useTexCoords)
                {
                    Assert(texCoordIndex > 0);
                    texCoordIndex -= 1;
                    Assert(texCoordIndex < maxVertices);
                    vertexTextureCoordinates[vertexCount] =
                        texCoords[texCoordIndex];
                }


                Assert(indexCount < maxIndices);
                indices[indexCount++] = vertexCount++;
            }

            Assert(indexCount % 3 == 0);
        }
        else if (fileData[cursor] == 's')
        {
            // Skip to next line
            while (fileData[cursor] != '\n')
            {
                cursor++;
            }
        }
        else
        {
            // Unknown syntax ignore field
            while (fileData[cursor++] != '\n')
            {
                // Skip entire line
            }
            continue;
            //return false;
        }
    }

    MeshData result = {};
    result.positions = AllocateArray(meshDataArena, vec3, vertexCount);
    result.normals = AllocateArray(meshDataArena, vec3, vertexCount);
    result.textureCoordinates = AllocateArray(meshDataArena, vec2, vertexCount);
    result.indices = AllocateArray(meshDataArena, u32, indexCount);

    CopyMemory(result.positions, vertexPositions, sizeof(vec3) * vertexCount);
    CopyMemory(result.normals, vertexNormals, sizeof(vec3) * vertexCount);
    CopyMemory(result.textureCoordinates, vertexTextureCoordinates,
        sizeof(vec2) * vertexCount);
    CopyMemory(result.indices, indices, sizeof(u32) * indexCount);
    result.vertexCount = vertexCount;
    result.indexCount = indexCount;
    return result;
}

struct Vertex
{
    vec3 position;
    vec3 normal;
    vec3 tangent;
    vec2 texCoord;
};

MeshData CreateCubeMeshData(MemoryArena *tempArena)
{
    // clang-format off
    Vertex cubeVertices[] = {
        // Top
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Bottom
        {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Back
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},

        // Front
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Left
        {{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},

        // Right
        {{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
    };

    u32 cubeIndices[] = {
        2, 1, 0,
        0, 3, 2,

        4, 5, 6,
        6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        14, 13, 12,
        12, 15, 14,

        16, 17, 18,
        18, 19, 16,

        22, 21, 20,
        20, 23, 22
    };

    MeshData result = {};
    result.vertexCount = ArrayCount(cubeVertices);
    result.indexCount = ArrayCount(cubeIndices);

    result.positions = AllocateArray(tempArena, vec3, result.vertexCount);
    result.normals = AllocateArray(tempArena, vec3, result.vertexCount);
    result.textureCoordinates = AllocateArray(tempArena, vec2, result.vertexCount);
    result.indices = AllocateArray(tempArena, u32, result.indexCount);

    CopyMemory(result.indices, cubeIndices, sizeof(u32) * result.indexCount);

    for (u32 i = 0; i < result.vertexCount; ++i)
    {
        result.positions[i] = cubeVertices[i].position;
        result.normals[i] = cubeVertices[i].normal;
        result.textureCoordinates[i] = cubeVertices[i].texCoord;
    }

    return result;
}

internal PixelBuffer LoadTexture(void *fileData, u32 length, MemoryArena *tempArena,
        b32 flipYAxis = true)
{
    PixelBuffer result = {};
    i32 width, height, channels;
    stbi_set_flip_vertically_on_load(flipYAxis);

    u8 *pixels = stbi_load_from_memory((const u8 *)fileData, length, &width,
            &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        // FIXME: Not all textures are 32BPP
        u32 bufferSize = width * height * 4;
        result.pixels = MemoryArena_Allocate(tempArena, bufferSize);

        u32 bytesPerPixel = 4;
        u32 pitch = width * 4;
        for (i32 y = 0; y < height; ++y)
        {
            for (i32 x = 0; x < width; ++x)
            {
                u32 src = *(u32*)(pixels + y * pitch + x * bytesPerPixel);
                u32 *dst = (u32*)((u8*)result.pixels + y * pitch + x * bytesPerPixel);

                u8 a = (src & 0xFF000000) >> 24;
                u8 b = (src & 0x00FF0000) >> 16;
                u8 g = (src & 0x0000FF00) >> 8;
                u8 r = (src & 0x000000FF);

                *dst = (a << 24) | (r << 16) | (g << 8) | b;
            }
        }
        //memcpy(result.pixels, pixels, bufferSize);

        stbi_image_free(pixels);

        result.width = width;
        result.height = height;
        result.pitch = width * 4;
        result.bytesPerPixel = 4;
    }

    return result;
}
