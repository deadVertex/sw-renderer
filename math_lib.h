#pragma once

#include <math.h>
#include <float.h>

#define EPSILON FLT_EPSILON
#define F32_MAX FLT_MAX

inline b32 Equal(f32 a, f32 b)
{
    f32 diff = a - b;
    b32 result = (diff > -FLT_EPSILON && diff <= FLT_EPSILON);
    return result;
}

inline f32 Sign(f32 a)
{
    f32 result = signbit(a) ? -1.0f : 1.0f;
    return result;
}

inline f32 Max(f32 a, f32 b)
{
    f32 result = (a > b) ? a : b;
    return result;
}

inline f32 Min(f32 a, f32 b)
{
    f32 result = (a < b) ? a : b;
    return result;
}

inline i32 Max(i32 a, i32 b)
{
    i32 result = (a > b) ? a : b;
    return result;
}

inline i32 Min(i32 a, i32 b)
{
    i32 result = (a < b) ? a : b;
    return result;
}

inline u32 MaxU(u32 a, u32 b)
{
    u32 result = (a > b) ? a : b;
    return result;
}

inline u32 MinU(u32 a, u32 b)
{
    u32 result = (a < b) ? a : b;
    return result;
}

union vec2
{
    struct
    {
        f32 x;
        f32 y;
    };

    f32 data[2];
};

union vec3
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
    };

    struct
    {
        vec2 xy;
        f32 _z;
    };

    f32 data[3];
};

union vec4
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
        f32 w;
    };

    struct
    {
        f32 r;
        f32 g;
        f32 b;
        f32 a;
    };

    struct
    {
        vec3 xyz;
        f32 _w;
    };

    f32 data[4];
};

union mat4
{
    vec4 columns[4];
    f32 data[16];
};

inline vec2 Vec2(f32 x, f32 y)
{
    vec2 result = {x, y};
    return result;
}

inline vec3 Vec3(f32 x, f32 y, f32 z)
{
    vec3 result = {x, y, z};
    return result;
}

inline vec3 Vec3(f32 x)
{
    vec3 result = Vec3(x, x, x);
    return result;
}

inline vec3 Vec3(vec2 v, f32 z)
{
    vec3 result = Vec3(v.x, v.y, z);
    return result;
}

inline vec4 Vec4(f32 x, f32 y, f32 z, f32 w)
{
    vec4 result = {x, y, z, w};
    return result;
}

inline vec4 Vec4(vec3 v, f32 w)
{
    vec4 result = {v.x, v.y, v.z, w};
    return result;
}

inline vec4 Vec4(f32 v)
{
    vec4 result = { v, v, v, v};
    return result;
}

inline f32 Radians(f32 x)
{
    f32 result = x * (PI / 180.0f);
    return result;
}

inline vec2 operator+(vec2 a, vec2 b)
{
    vec2 result;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    return result;
}

inline vec2 operator-(vec2 a)
{
    vec2 result = {-a.x, -a.y};
    return result;
}

inline vec2 operator-(vec2 a, vec2 b)
{
    vec2 result = a + -b;
    return result;
}

inline vec2& operator+=(vec2 &a, vec2 b)
{
    a = a + b;
    return a;
}

inline vec2 operator*(vec2 a, f32 b)
{
    vec2 result = {};
    result.x = a.x * b;
    result.y = a.y * b;

    return result;
}

inline vec3 operator-(vec3 a)
{
    vec3 result;
    result.x = -a.x;
    result.y = -a.y;
    result.z = -a.z;

    return result;
}

inline vec3 operator+(vec3 a, vec3 b)
{
    vec3 result;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    result.z = a.z + b.z;
    return result;
}

inline vec3& operator+=(vec3 &a, vec3 b)
{
    a = a + b;
    return a;
}

inline vec3 operator-(vec3 a, vec3 b)
{
    vec3 result = a + -b;
    return result;
}

inline vec3& operator-=(vec3 &a, vec3 b)
{
    a = a - b;
    return a;
}

inline vec3 operator*(vec3 a, f32 b)
{
    vec3 result = {};
    result.x = a.x * b;
    result.y = a.y * b;
    result.z = a.z * b;

    return result;
}

inline vec3 operator*(f32 a, vec3 b)
{
    vec3 result = {};
    result.x = b.x * a;
    result.y = b.y * a;
    result.z = b.z * a;

    return result;
}

inline vec3& operator*=(vec3 &a, f32 b)
{
    a = a * b;
    return a;
}

inline vec3 operator/(f32 a, vec3 b)
{
    vec3 result;
    result.x = a / b.x;
    result.y = a / b.y;
    result.z = a / b.z;
    return result;
}

inline vec4 operator+(vec4 a, vec4 b)
{
    vec4 result = {};
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    result.z = a.z + b.z;
    result.w = a.w + b.w;

    return result;
}

inline vec4 operator*(vec4 a, f32 b)
{
    vec4 result = {};
    result.x = a.x * b;
    result.y = a.y * b;
    result.z = a.z * b;
    result.w = a.w * b;

    return result;
}

inline f32 Dot(vec3 a, vec3 b)
{
    f32 result = a.x * b.x + a.y * b.y + a.z * b.z;
    return result;
}

inline vec3 Cross(vec3 a, vec3 b)
{
    vec3 result;

    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);

    return result;

}

inline vec3 Hadamard(vec3 a, vec3 b)
{
    vec3 result;
    result.x = a.x * b.x;
    result.y = a.y * b.y;
    result.z = a.z * b.z;

    return result;
}

inline f32 LengthSq(vec3 v)
{
    f32 result = Dot(v, v);
    return result;
}

inline f32 Sqrt(f32 x)
{
    f32 result = sqrtf(x);
    return result;
}

inline f32 Pow(f32 x, f32 y)
{
    f32 result = powf(x, y);
    return result;
}

inline f32 Length(vec3 v)
{
    f32 result = LengthSq(v);
    result = Sqrt(result);
    return result;
}

inline vec3 Normalize(vec3 v)
{
    f32 invLength = 1.0f / Length(v);
    vec3 result = v * invLength;
    return result;
}

inline vec3 SafeNormalize(vec3 v)
{
    vec3 result = v;
    if (LengthSq(v) > EPSILON)
    {
        result = Normalize(v);
    }

    return result;
}

inline b32 Equal(vec3 a, vec3 b)
{
    b32 result = Equal(a.x, b.x) && Equal(a.y, b.y) && Equal(a.z, b.z);
    return result;
}

inline vec3 Min(vec3 a, vec3 b)
{
    vec3 result = {};
    result.x = Min(a.x, b.x);
    result.y = Min(a.y, b.y);
    result.z = Min(a.z, b.z);

    return result;
}

inline vec3 Max(vec3 a, vec3 b)
{
    vec3 result = {};
    result.x = Max(a.x, b.x);
    result.y = Max(a.y, b.y);
    result.z = Max(a.z, b.z);

    return result;
}

inline f32 Lerp(f32 a, f32 b, f32 t)
{
    f32 result = a * (1.0f - t) + b * t;
    return result;
}

inline vec3 Lerp(vec3 a, vec3 b, f32 t)
{
    vec3 result = a * (1.0f - t) + b * t;
    return result;
}

inline vec4 Lerp(vec4 a, vec4 b, f32 t)
{
    vec4 result = a * (1.0f - t) + b * t;
    return result;
}

inline f32 Fmod(f32 nom, f32 denom)
{
    f32 result = fmodf(nom, denom);
    return result;
}

inline f32 Floor(f32 x)
{
    f32 result = floorf(x);
    return result;
}

inline f32 Ceil(f32 x)
{
    f32 result = ceilf(x);
    return result;
}

inline f32 Fract(f32 x)
{
    f32 i = Floor(x);
    f32 result = x - i;
    return result;
}

inline f32 Sin(f32 radians)
{
    f32 result = sinf(radians);
    return result;
}

inline f32 Cos(f32 radians)
{
    f32 result = cosf(radians);
    return result;
}

inline f32 Tan(f32 radians)
{
    f32 result = tanf(radians);
    return result;
}

inline f32 Abs(f32 x)
{
    f32 result = fabsf(x);
    return result;
}

inline f32 Clamp(f32 x, f32 min, f32 max)
{
    if (x < min)
    {
        x = min;
    }
    if (x > max)
    {
        x = max;
    }

    return x;
}

inline i32 Clamp(i32 x, i32 min, i32 max)
{
    if (x < min)
    {
        x = min;
    }
    if (x > max)
    {
        x = max;
    }

    return x;
}

inline f32 Clamp01(f32 x)
{
    f32 result = Clamp(x, 0.0f, 1.0f);
    return result;
}

inline f32 MapToUnitRange(f32 min, f32 max, f32 t)
{
    f32 range = max - min;
    f32 result = Clamp01((t - min) / range);
    return result;
}


inline mat4 Perspective(f32 fovy, f32 aspect, f32 zNear, f32 zFar)
{
    f32 radians = Radians(fovy);
    f32 scale = Tan(radians * 0.5f) * zNear;

    f32 right = aspect * scale;
    f32 left = -right;

    f32 top = scale;
    f32 bottom = -scale;

    mat4 result = {};
    result.columns[0].x = (2.0f * zNear) / (right - left);
    result.columns[1].y = (2.0f * zNear) / (top - bottom);
    result.columns[2].x = (right + left) / (right - left);
    result.columns[2].y = (top + bottom) / (top - bottom);
    result.columns[2].z = -(zFar + zNear) / (zFar - zNear);
    result.columns[2].w = -1.0f;
    result.columns[3].z = -(2.0f * zFar * zNear) / (zFar - zNear);

    return result;
}

inline mat4 Orthographic(
    f32 left, f32 right, f32 bottom, f32 top, f32 zNear = 0.0f, f32 zFar = 1.0f)
{
    mat4 result = {};

    result.data[0] = 2.0f / (right - left);
    result.data[5] = 2.0f / (top - bottom);
    result.data[10] = -2.0f / (zFar - zNear);
    result.data[12] = -(right + left) / (right - left);
    result.data[13] = -(top + bottom) / (top - bottom);
    result.data[14] = -(zFar + zNear) / (zFar - zNear);
    result.data[15] = 1.0f;

    return result;
}

inline mat4 Identity()
{
    mat4 result = {};
    result.data[0] = 1.0f;
    result.data[5] = 1.0f;
    result.data[10] = 1.0f;
    result.data[15] = 1.0f;

    return result;
}

inline mat4 Translate(vec3 translation)
{
    mat4 result = Identity();

    result.columns[3].x = translation.x;
    result.columns[3].y = translation.y;
    result.columns[3].z = translation.z;

    return result;
}

inline mat4 RotateX(f32 radians)
{
    mat4 result = Identity();

    result.columns[1].y = Cos(radians);
    result.columns[1].z = Sin(radians);
    result.columns[2].y = -Sin(radians);
    result.columns[2].z = Cos(radians);

    return result;
}

inline mat4 RotateY(f32 radians)
{
    mat4 result = Identity();

    result.columns[0].x = Cos(radians);
    result.columns[0].z = -Sin(radians);
    result.columns[2].x = Sin(radians);
    result.columns[2].z = Cos(radians);

    return result;
}

inline mat4 RotateZ(f32 radians)
{
    mat4 result = Identity();

    result.columns[0].x = Cos(radians);
    result.columns[0].y = Sin(radians);
    result.columns[1].x = -Sin(radians);
    result.columns[1].y = Cos(radians);

    return result;
}

inline mat4 Scale(vec3 scaling)
{
    mat4 result = {};
    result.columns[0].x = scaling.x;
    result.columns[1].y = scaling.y;
    result.columns[2].z = scaling.z;
    result.columns[3].w = 1.0f;

    return result;
}

inline mat4 WorldTransform(vec3 translation, vec3 scaling)
{
    mat4 result = {};
    result.columns[0].x = scaling.x;
    result.columns[1].y = scaling.y;
    result.columns[2].z = scaling.z;
    result.columns[3].w = 1.0f;

    result.columns[3].x = translation.x;
    result.columns[3].y = translation.y;
    result.columns[3].z = translation.z;

    return result;
}

inline f32 Dot(vec4 a, vec4 b)
{
    // clang-format off
    f32 result = a.data[0] * b.data[0] +
                 a.data[1] * b.data[1] +
                 a.data[2] * b.data[2] +
                 a.data[3] * b.data[3];
    // clang-format on

    return result;
}

inline mat4 operator*(mat4 a, mat4 b)
{
    mat4 result = {};

    for (u32 i = 0; i < 4; ++i)
    {
        // clang-format off
        vec4 row = Vec4(a.columns[0].data[i],
                        a.columns[1].data[i],
                        a.columns[2].data[i],
                        a.columns[3].data[i]);

        result.columns[0].data[i] = Dot(row, b.columns[0]);
        result.columns[1].data[i] = Dot(row, b.columns[1]);
        result.columns[2].data[i] = Dot(row, b.columns[2]);
        result.columns[3].data[i] = Dot(row, b.columns[3]);
        // clang-format on
    }

    return result;
}

inline vec4 operator*(mat4 a, vec4 v)
{
    vec4 result = {};

    for (u32 i = 0; i < 4; ++i)
    {
        // clang-format off
        vec4 row = Vec4(a.columns[0].data[i],
                        a.columns[1].data[i],
                        a.columns[2].data[i],
                        a.columns[3].data[i]);

        result.data[i] = Dot(row, v);
        // clang-format on
    }

    return result;
}

inline mat4 ChangeOfBasis(vec3 x)
{
    vec3 z = Cross(x, Vec3(0, 1, 0));
    if (LengthSq(z) < EPSILON)
    {
        z = Cross(Vec3(1, 0, 0), x);
        Assert(LengthSq(z) > EPSILON);
    }
    z = Normalize(z);

    vec3 y = Normalize(Cross(z, x));

    mat4 result;
    result.columns[0] = Vec4(x, 0.0f);
    result.columns[1] = Vec4(y, 0.0f);
    result.columns[2] = Vec4(z, 0.0f);
    result.columns[3] = Vec4(0, 0, 0, 1);

    return result;
}

inline mat4 Transpose(mat4 a)
{
    mat4 result;
    result.data[0] = a.data[0];
    result.data[1] = a.data[4];
    result.data[2] = a.data[8];
    result.data[3] = a.data[12];

    result.data[4] = a.data[1];
    result.data[5] = a.data[5];
    result.data[6] = a.data[9];
    result.data[7] = a.data[13];

    result.data[8] = a.data[2];
    result.data[9] = a.data[6];
    result.data[10] = a.data[10];
    result.data[11] = a.data[14];

    result.data[12] = a.data[3];
    result.data[13] = a.data[7];
    result.data[14] = a.data[11];
    result.data[15] = a.data[15];

    return result;
}

struct rect2
{
    vec2 min;
    vec2 max;
};

inline rect2 RectMinMax2(vec2 min, vec2 max)
{
    rect2 result;
    result.min = min;
    result.max = max;
    return result;
}

inline rect2 Rect2(vec2 position, vec2 dimensions)
{
    rect2 result;
    result.min = position;
    result.max = position + dimensions;
    return result;
}

inline b32 ContainsPoint(rect2 rect, vec2 p)
{
    if ((p.x >= rect.min.x) && (p.x <= rect.max.x))
    {
        if ((p.y >= rect.min.y) && (p.y <= rect.max.y))
        {
            return true;
        }
    }
    return false;
}

inline rect2 Union(rect2 a, rect2 b)
{
    rect2 result = {};
    result.min.x = Min(a.min.x, b.min.x);
    result.min.y = Min(a.min.y, b.min.y);
    result.max.x = Max(a.max.x, b.max.x);
    result.max.y = Max(a.max.y, b.max.y);

    return result;
}

union quat
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
        f32 w;
    };

    struct
    {
        vec3 v;
        f32 s;
    };
};

inline quat Quat()
{
    quat result = {};
    result.s = 1.0f;
    return result;
}

inline quat Quat(vec3 axis, float angle)
{
    quat result;
    float halfAngle = angle * 0.5f;
    result.v = axis * Sin(halfAngle);
    result.s = Cos(halfAngle);
    return result;
}

inline quat Quat(float x, float y, float z, float w)
{
    quat result;
    result.v = Vec3(x, y, z);
    result.s = w;
    return result;
}

inline quat operator*(quat p, quat q)
{
    quat result;
    result.v = p.s * q.v + q.s * p.v + Cross(p.v, q.v);
    result.s = p.s * q.s - Dot(p.v, q.v);
    return result;
}

inline quat& operator*=(quat &p, quat q)
{
    p = p * q;
    return p;
}

inline quat Inverse(quat p)
{
    quat result;
    result.v = -p.v;
    result.s = p.s;
    return result;
}

inline quat Normalize(quat p)
{
    quat result;
    float lengthSq = LengthSq(p.v) + p.s * p.s;
    float invLength = 1.0f / Sqrt(lengthSq);
    result.x = p.x * invLength;
    result.y = p.y * invLength;
    result.z = p.z * invLength;
    result.w = p.w * invLength;

    return result;
}

inline quat Lerp(quat p, quat q, f32 t)
{
    quat result;
    result.x = Lerp(p.x, q.x, t);
    result.y = Lerp(p.y, q.y, t);
    result.z = Lerp(p.z, q.z, t);
    result.w = Lerp(p.w, q.w, t);
    return Normalize(result);
}


inline vec3 Rotate(quat rotation, vec3 v)
{
    quat p = Quat(v.x, v.y, v.z, 0.0f);
    quat q = rotation * p * Inverse(rotation);
    vec3 result = Vec3(q.x, q.y, q.z);

    return result;
}

inline mat4 Rotate(quat rotation)
{
  mat4 result = Identity();
  float x = rotation.x;
  float y = rotation.y;
  float z = rotation.z;
  float w = rotation.w;

  result.columns[0].x = 1.0f - 2.0f * y * y - 2.0f * z * z;
  result.columns[0].y = 2.0f * x * y + 2.0f * z * w;
  result.columns[0].z = 2.0f * x * z - 2.0f * y * w;

  result.columns[1].x = 2.0f * x * y - 2.0f * z * w;
  result.columns[1].y = 1.0f - 2.0f * x * x - 2.0f * z * z;
  result.columns[1].z = 2.0f * y * z + 2.0f * x * w;

  result.columns[2].x = 2.0f * x * z + 2.0f * y * w;
  result.columns[2].y = 2.0f * y * z - 2.0f * x * w;
  result.columns[2].z = 1.0f - 2.0f * x * x - 2.0f * y * y;

  return result;
}

// FIXME: From wikipedia, no idea how it works
inline vec3 ToEulerAngles(quat q)
{
    vec3 result;
    f32 sinXCosY = 2.0f * (q.s * q.x + q.y * q.z);
    f32 cosXCosY = 1.0f - 2.0f * (q.x * q.x + q.y * q.y);
    result.x = atan2f(sinXCosY, cosXCosY);

    f32 sinY = 2.0f * (q.s * q.y - q.z * q.x);
    result.y = (Abs(sinY) >= 1.0f) ? (f32)copysign(PI * 0.5f, sinY) : (f32)asin(sinY);

    f32 sinZCosY = 2.0f * (q.w * q.z + q.x * q.y);
    f32 cosZCosY = 1.0f - 2.0f * (q.y * q.y + q.z * q.z);
    result.z = atan2f(sinZCosY, cosZCosY);

    return result;
}

// FIXME: Same as ToEulerAngles
inline quat FromEulerAngles(vec3 eulerAngles)
{
    f32 cz = Cos(eulerAngles.z * 0.5f);
    f32 sz = Sin(eulerAngles.z * 0.5f);
    f32 cy = Cos(eulerAngles.y * 0.5f);
    f32 sy = Sin(eulerAngles.y * 0.5f);
    f32 cx = Cos(eulerAngles.x * 0.5f);
    f32 sx = Sin(eulerAngles.x * 0.5f);

    quat result;
    result.s = cz * cy * cx + sz * sy * sx;
    result.v.x = cz * cy * sx - sz * sy * cx;
    result.v.y = sz * cy * sx + cz * sy * cx;
    result.v.z = sz * cy * cx - cz * sy * sx;
    return result;
}

inline void AxisAngle(quat q, vec3 *axis, f32 *angle)
{
    f32 halfAngle = acosf(q.s);
    *angle = halfAngle * 2.0f;
    f32 invSinHalfAngle = 1.0f / Sin(halfAngle);
    *axis = q.v * invSinHalfAngle;
}

inline u32 GetLargestAxis(vec3 v)
{
    u32 largestAxis = 0;
    for (u32 i = 1; i < 3; ++i)
    {
        if (Abs(v.data[i]) > Abs(v.data[largestAxis]))
        {
            largestAxis = i;
        }
    }

    return largestAxis;
}

inline vec3 TransformPoint(vec3 p, mat4 m)
{
    vec4 v = m * Vec4(p, 1.0);
    return v.xyz;
}

inline vec3 TransformVector(vec3 v, mat4 m)
{
    vec4 result = m * Vec4(v, 0.0);
    return result.xyz;
}

inline mat4 CalculateLocalToWorldMatrix(
    vec3 position, quat rotation, vec3 scale)
{
    mat4 result = Translate(position) * Rotate(rotation) * Scale(scale);

    return result;
}

inline f32 Max(vec3 v)
{
    f32 result = Max(v.x, Max(v.y, v.z));
    return result;
}

inline f32 Min(vec3 v)
{
    f32 result = Min(v.x, Min(v.y, v.z));
    return result;
}

inline vec3 Abs(vec3 v)
{
    vec3 result = {Abs(v.x), Abs(v.y), Abs(v.z)};
    return result;
}

#ifndef SIMD_RNG
struct RandomNumberGenerator
{
    u32 state;
};

// Reference implementation from https://en.wikipedia.org/wiki/Xorshift
inline u32 XorShift32(RandomNumberGenerator *rng)
{
    u32 x = rng->state;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
    rng->state = x;
	return x;
}

inline f32 RandomUnilateral(RandomNumberGenerator *rng)
{
    // Clear the sign bit as SSE interprets integers as signed when converting
    // to packed single
    f32 numerator = (f32)(XorShift32(rng) >> 1);
    f32 denom = (f32)(U32_MAX >> 1);
    f32 result = numerator / denom;
    return result;
}

inline f32 RandomBilateral(RandomNumberGenerator *rng)
{
    f32 result = -1.0f + 2.0f * RandomUnilateral(rng);
    return result;
}
#endif
