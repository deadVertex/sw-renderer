#include <dlfcn.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdarg.h>

struct DebugReadFileResult
{
    void *contents;
    u32 size;
};

inline void LogMessage(const char *fmt, ...)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
    puts(buffer);
}

internal void OS_FreeMemory(void *p);
internal void *OS_AllocateMemory(u64 numBytes, u64 baseAddress = 0);

internal void *OS_AllocateMemory(u64 numBytes, u64 baseAddress)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void OS_FreeMemory(void *p)
{
    munmap(p, 0);
}

internal void OS_FreeFileMemory(DebugReadFileResult fileResult)
{
    OS_FreeMemory(fileResult.contents);
}

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile(int file, void *buf, int bytesToRead)
{
    while (bytesToRead)
    {
        int bytesRead = read(file, buf, bytesToRead);
        if (bytesRead == -1)
        {
            return false;
        }
        bytesToRead -= bytesRead;
        buf = (u8 *)buf + bytesRead;
    }
    return true;
}

internal DebugReadFileResult OS_ReadEntireFile(const char *path)
{
    DebugReadFileResult result = {};
    int file = open(path, O_RDONLY);
    if (file != -1)
    {
        struct stat fileStatus;
        if (fstat(file, &fileStatus) != -1)
        {
            result.size = SafeTruncateU64ToU32(fileStatus.st_size);
            // NOTE: Size + 1 to allow for null to be written if needed.
            result.contents = OS_AllocateMemory(result.size + 1);
            if (result.contents)
            {
                if (!ReadFile(file, result.contents, result.size))
                {
                    LogMessage("Failed to read file %s", path);
                    OS_FreeMemory(result.contents);
                    result.contents = nullptr;
                    result.size = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.size, path);
                result.size = 0;
            }
        }
        else
        {
            LogMessage("Failed to read file size for file %s", path);
        }
        close(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}
